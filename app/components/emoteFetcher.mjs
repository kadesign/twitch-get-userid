export default class EmoteFetcher {
  #apiUrls = {
    sevenTv: {
      user: id => `https://7tv.io/v3/users/twitch/${id}`,
      global: 'https://7tv.io/v3/emote-sets/global'
    }
  };

  #emotes = new Map();

  get emotes() {
    return Array.from(this.#emotes.values());
  }

  clearEmotes() {
    this.#emotes.clear();
  }

  async fetchSevenTvEmotes(channelId) {
    try {
      if (channelId) {
        const response = await this.#callApi(this.#apiUrls.sevenTv.user(channelId), '7TV');
        response.emote_set.emotes.forEach(emote => this.#emotes.set(emote.name, {
          id: emote.id,
          code: emote.name,
          type: '7tv'
        }));
      } else {
        const response = await this.#callApi(this.#apiUrls.sevenTv.global, '7TV');
        response.emotes.forEach(emote => this.#emotes.set(emote.name, {
          id: emote.id,
          code: emote.name,
          type: '7tv'
        }));
      }
    } catch (e) {
      console.error(e);
    }
  }

  #callApi(url, service) {
    return fetch(url, {
      method: 'GET'
    }).then(response => {
      if (response.ok) {
        try {
          return response.json();
        } catch (e) {
          console.error(e);
          return new Promise((resolve, reject) => reject(`Can't parse response from ${service} API`));
        }
      } else {
        return new Promise((resolve, reject) => reject(`${service} API returned an error`));
      }
    })
  }
}
